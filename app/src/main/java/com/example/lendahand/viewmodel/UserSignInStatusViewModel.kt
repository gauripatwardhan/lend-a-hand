package com.example.lendahand.viewmodel

import androidx.lifecycle.ViewModel
import com.example.lendahand.Filters

class UserSignInStatusViewModel : ViewModel() {
    private var mIsSigningIn: Boolean = false
    private var mFilters: Filters = Filters.getDefault()


    fun getIsSigningIn(): Boolean {
        return mIsSigningIn
    }

    fun setIsSigningIn(mIsSigningIn: Boolean) {
        this.mIsSigningIn = mIsSigningIn
    }

    fun getFilters(): Filters? {
        return mFilters
    }

    fun setFilters(mFilters: Filters?) {
        this.mFilters = mFilters!!
    }
}

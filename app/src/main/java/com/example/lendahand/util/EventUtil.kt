package com.example.lendahand.util

import android.content.Context
import com.example.lendahand.FireStoreInstance
import com.example.lendahand.R
import com.example.lendahand.model.Event
import java.util.*
import java.util.concurrent.LinkedBlockingQueue
import java.util.concurrent.ThreadPoolExecutor
import java.util.concurrent.TimeUnit

/**
 * Utilities for Events.
 */
class EventUtil {
    companion object {
        private val TAG = "EventUtil"
        private val EXECUTOR =
            ThreadPoolExecutor(
                2,
                4,
                60,
                TimeUnit.SECONDS,
                LinkedBlockingQueue()
            )
        private val RESTAURANT_URL_FMT =
            "https://storage.googleapis.com/firestorequickstarts.appspot.com/food_%d.png"
        private val MAX_IMAGE_NUM = 22
        private val NAME_FIRST_WORDS = arrayOf(
            "Foo",
            "Bar",
            "Baz",
            "Qux",
            "Fire",
            "Sam's",
            "World Famous",
            "Google",
            "The Best"
        )
        private val NAME_SECOND_WORDS = arrayOf(
            "Event",
            "Cafe",
            "Spot",
            "Eatin' Place",
            "Eatery",
            "Drive Thru",
            "Diner"
        )

        /**
         * Create a random Event POJO.
         */
        fun getRandom(context: Context): Event {
            val event = Event()
            val random = Random()
            // Cities (first element is 'Any')
            var cities =
                context.resources.getStringArray(R.array.cities)
            cities = Arrays.copyOfRange(cities, 1, cities.size)
            // Categories (first element is 'Any')
            var categories =
                context.resources.getStringArray(R.array.categories)
            categories = Arrays.copyOfRange(categories, 1, categories.size)
            // Create a random time for the event
            val timestamp = Calendar.getInstance()
            timestamp.set(random.nextInt(2050), random.nextInt(12), random.nextInt(31),
                random.nextInt(24), random.nextInt(60), 0)

            event.setName(getRandomName(random))
            event.setCity(getRandomString(cities, random))
            event.setCategory(getRandomString(categories, random))
            event.setPhoto(getRandomImageUrl(random))
            event.setTimeStampEvent(timestamp.timeInMillis)
            event.setAvgRating(getRandomRating(random))
            event.setNumRatings(random.nextInt(20))
            event.setUserId(FireStoreInstance.userId)
            event.setUserName("Default")
            return event
        }

        /**
         * Create a Event POJO.
         */
        fun createEvent(context: Context, eventName: String, description: String, registration: String,
                             timestamp: Long, phone: String, city: String, state: String,
                             zipcode: Long, category: String, userId: String, username: String,
                             photo: String): Event {
            val event = Event()
            val random = Random()
            event.setName(eventName)
            event.setDescription(description)
            event.setRegistrationLink(registration)
            event.setTimeStampEvent(timestamp)
            event.setPhone(phone)
            event.setCity(city)
            event.setState(state)
            event.setZipcode(zipcode)
            event.setAvgRating(0.0)
            event.setNumRatings(0)
            event.setCategory(category)
            event.setUserId(userId)
            event.setPhoto(photo)
            event.setUserName(username)
            return event
        }


        /**
         * Get a random image.
         */
        private fun getRandomImageUrl(random: Random): String { // Integer between 1 and MAX_IMAGE_NUM (inclusive)
            val id = random.nextInt(MAX_IMAGE_NUM) + 1
            return String.format(
                Locale.getDefault(),
                RESTAURANT_URL_FMT,
                id
            )
        }

        /**
         * Get date
         */
        fun getDateString(event: Event): String {
            return event.getDateString()!!
        }

        /**
         * Get time
         */
        fun getTimeString(event: Event): String {
            return event.getTimeString()!!
        }

        private fun getRandomRating(random: Random): Double {
            val min = 1.0
            return min + random.nextDouble() * 4.0
        }

        private fun getRandomName(random: Random): String {
            return (getRandomString(NAME_FIRST_WORDS, random) + " "
                    + getRandomString(NAME_SECOND_WORDS, random))
        }

        private fun getRandomString(
            array: Array<String>,
            random: Random
        ): String {
            val ind = random.nextInt(array.size)
            return array[ind]
        }

        private fun getRandomInt(array: IntArray, random: Random): Int {
            val ind = random.nextInt(array.size)
            return array[ind]
        }
    }
}
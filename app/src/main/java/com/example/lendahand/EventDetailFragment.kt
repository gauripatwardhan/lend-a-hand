package com.example.lendahand

import android.content.Context
import android.os.Bundle
import android.provider.CalendarContract
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.lendahand.RatingDialogFragment.RatingListener
import com.example.lendahand.adapter.RatingAdapter
import com.example.lendahand.databinding.EventDetailBinding
import com.example.lendahand.model.Event
import com.example.lendahand.model.Rating
import com.example.lendahand.model.RatingsStat
import com.google.android.gms.tasks.OnFailureListener
import com.google.android.gms.tasks.OnSuccessListener
import com.google.android.gms.tasks.Task
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.UserInfo
import com.google.firebase.firestore.*
import me.zhanghai.android.materialratingbar.MaterialRatingBar

class EventDetailFragment : Fragment(),
    View.OnClickListener,
    EventListener<DocumentSnapshot>, RatingListener,
    MainActivity.OnRatingSelectedListener {

    private var mImageView: ImageView? = null
    private var mNameView: TextView? = null
    private var mRatingIndicator: MaterialRatingBar? = null
    private var mNumRatingsView: TextView? = null
    private var mCityView: TextView? = null
    private var mStateView: TextView? = null
    private var mZipcodeView: TextView? = null
    private var mCategoryView: TextView? = null
    private var mDateView: TextView? = null
    private var mTimeView: TextView? = null
    private var mRegistrationView: TextView? = null
    private var mContactView: TextView? = null
    private var mDescriptionView: TextView? = null
    private var mContactUser: TextView? = null
    private var mEventReportedView: TextView? = null
    private var mWarningView: RelativeLayout? = null
    private var mEmptyView: ViewGroup? = null
    private var mRatingsRecycler: RecyclerView? = null
    private var mRatingDialog: RatingDialogFragment? = null
    private var mEventRef: DocumentReference? = null
    private var mEventRegistration: ListenerRegistration? = null
    private var mRatingAdapter: RatingAdapter? = null

    private var mEventUserId: String? = null

    private var mRatingsStatRef: DocumentReference? = null
    private var mRatingStat: RatingsStat? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding: EventDetailBinding = DataBindingUtil.inflate(
            inflater, R.layout.event_detail, container, false
        )

        // TODO: Make sure the user is signed in
        FirebaseAuth.getInstance().currentUser!!
        Log.d(TAG, "InitFirestore: Confirmed that user is logged in")

        mImageView = binding.eventImage
        mNameView = binding.eventName
        mRatingIndicator = binding.eventRating
        mNumRatingsView = binding.eventNumRatings
        mCityView = binding.eventCity
        mStateView = binding.eventState
        mZipcodeView = binding.eventZipcode
        mTimeView = binding.eventTime
        mDateView = binding.eventDate
        mRegistrationView = binding.registerLink
        mDescriptionView = binding.description
        mEventReportedView = binding.eventReported
        mWarningView = binding.warning
        mContactView = binding.contactNumber
        mStateView = binding.eventState
        mCategoryView = binding.eventCategory
        mEmptyView = binding.viewEmptyRatings
        mRatingsRecycler = binding.recyclerRatings
        mContactUser = binding.contactUser
        binding.eventButtonBack.setOnClickListener(this)
        binding.fabShowRatingDialog.setOnClickListener(this)

        // Set OnRatingSelected
        MainActivity.mCallbackRating = this
        // Get event ID from extras
        val eventId: String = arguments!!.getString(KEY_EVENT_ID)
            ?: throw IllegalArgumentException(("Must pass extra ").plus(KEY_EVENT_ID))
        Log.i(TAG, "Event id received: ".plus(eventId))

        // Initialize Firestore
        //FireStoreInstance.mFirestore = FirebaseFirestore.getInstance()

        // TODO: Make sure the user is signed in
        FirebaseAuth.getInstance().currentUser!!
        Log.d(TAG, "After getInstance (ii): Confirmed that user is logged in")


        // Get reference to the event
        mEventRef = FireStoreInstance.mFirestore!!.collection("events").document(eventId)
        // Get ratings
        val ratingsQuery = mEventRef!!
            .collection("ratings")
            .orderBy("timestamp", Query.Direction.DESCENDING)
            .limit(50)
        // RecyclerView
        mRatingAdapter = object : RatingAdapter(ratingsQuery) {
            override fun onDataChanged() {
                if (itemCount == 0) {
                    mRatingsRecycler!!.visibility = View.GONE
                    mEmptyView!!.visibility = View.VISIBLE
                } else {
                    mRatingsRecycler!!.visibility = View.VISIBLE
                    mEmptyView!!.visibility = View.GONE
                }
            }
        }
        mRatingsRecycler!!.layoutManager = LinearLayoutManager(this.context)
        mRatingsRecycler!!.adapter = mRatingAdapter
        mRatingDialog = RatingDialogFragment()

        // Set onClickListerner if the event is reported as fake
        mEventReportedView!!.setOnClickListener {view: View ->
            mEventReportedView!!.visibility = View.GONE
            mWarningView!!.visibility = View.VISIBLE
            // In a transaction, add the new rating and update the aggregate totals
            FireStoreInstance.mFirestore!!.runTransaction { transaction ->
                val event: Event? = transaction[mEventRef!!]
                    .toObject<Event>(Event::class.java)
                event!!.setReported()
                // Commit to Firestore
                transaction[mEventRef!!] = event!!
            }
        }

        return binding.root
    }

    override fun onStart() {
        super.onStart()
        mRatingAdapter!!.startListening()
        mEventRegistration = mEventRef!!.addSnapshotListener(this)
    }

    override fun onStop() {
        super.onStop()
        mRatingAdapter!!.stopListening()
        if (mEventRegistration != null) {
            mEventRegistration!!.remove()
            mEventRegistration = null
        }
    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.event_button_back -> onBackArrowClicked(v)
            R.id.fab_show_rating_dialog -> onAddRatingClicked(v)
        }
    }

    private fun addRating(
        eventRef: DocumentReference?,
        rating: Rating
    ): Task<Void> {
        // Create reference for new rating and ratingsStat, for use inside the transaction
        val ratingRef: DocumentReference = eventRef!!.collection("ratings").document()

        // In a transaction, add the new rating and update the aggregate totals
        return FireStoreInstance.mFirestore!!.runTransaction { transaction  ->
            val event: Event? = transaction[eventRef].toObject<Event>(Event::class.java)
            // Compute new number of ratings
            val newNumRatings: Int = event!!.getNumRatings() + 1
            // Compute new average rating
            val oldRatingTotal: Double = event.getAvgRating() *
                    event.getNumRatings()
            val newAvgRating: Double = (oldRatingTotal + rating.getRating()) /
                    newNumRatings
            // Set new event info
            event.setNumRatings(newNumRatings)
            event.setAvgRating(newAvgRating)
            // Commit to Firestore
            transaction[eventRef] = event
            transaction[ratingRef] = rating
            // Success
            null
        }
    }

    /**
     * Listener for the Event document ([.mEventRef]).
     */
    override fun onEvent(
        snapshot: DocumentSnapshot?,
        e: FirebaseFirestoreException?
    ) {
        if (e != null) {
            Log.w(TAG, "event:onEvent", e)
            return
        }
        onEventLoaded(snapshot!!.toObject(Event::class.java))
    }

    private fun onEventLoaded(event: Event?) {
        mNameView!!.text = event!!.getName()
        mCityView!!.text = event.getCity()
        mCategoryView!!.text = event.getCategory()
        mTimeView!!.text = event.getTimeString()
        mDateView!!.text = event.getDateString()
        mStateView!!.text = event.getState()
        mZipcodeView!!.text = event.getZipcode().toString()
        mDescriptionView!!.text = event.getDescription()
        mRatingIndicator!!.rating = event.getAvgRating().toFloat()
        mNumRatingsView!!.text = getString(R.string.fmt_num_ratings, event.getNumRatings())
        mEventUserId = event.getUserId()
        mContactUser!!.text = event.getUserName()

        if (!event.getRegistrationLink().equals("none", true)) {
            mRegistrationView!!.text = event.getRegistrationLink()
            mRegistrationView!!.visibility = View.VISIBLE
        }
        if (event.getReported()) {
            mEventReportedView!!.visibility = View.GONE
            mWarningView!!.visibility = View.VISIBLE
        }

        // Show contact number only when the textview is clicked
        mContactView!!.setOnClickListener {view: View ->
            mContactView!!.text = event.getPhone().toString()
        }

        // Background image
        Log.d("EDF: Photo link: ", event.getPhoto())
        Glide.with(mImageView!!.context)
            .load(event.getPhoto())
            .into(mImageView!!)
    }

    fun onBackArrowClicked(view: View?) {
        this.activity!!.onBackPressed()
    }

    fun onAddRatingClicked(view: View?) {
        mRatingDialog!!.show(this.activity!!.supportFragmentManager, RatingDialogFragment.TAG)
    }


    override fun onRating(rating: Rating) {
        onRatingSelected(rating)
    }

    override fun onRatingSelected(rating: Rating) { // In a transaction, add the new rating and update the aggregate totals
        if (rating.getUserId().equals(mEventUserId)) { // Owner of event cannot rate their own event
            Snackbar.make(
                    this.activity!!.findViewById(android.R.id.content), "You cannot rate your own event!",
            Snackbar.LENGTH_SHORT
            ).show()
            return
        }
        addRating(mEventRef, rating)
            .addOnSuccessListener(this.activity!!, OnSuccessListener<Void?> {
                Log.d(TAG, "Rating added")
                // Hide keyboard and scroll to top
                hideKeyboard()
                mRatingsRecycler!!.smoothScrollToPosition(0)
            })
            .addOnFailureListener(this.activity!!, OnFailureListener { e ->
                Log.w(TAG, "Add rating failed", e)
                // Show failure message and hide keyboard
                hideKeyboard()
                Snackbar.make(
                    this.activity!!.findViewById(android.R.id.content), "Failed to add rating",
                    Snackbar.LENGTH_SHORT
                ).show()
            })
    }

    private fun hideKeyboard() {

        val imm =
            activity!!.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(view!!.windowToken, 0)
    }

    companion object {
        private const val TAG = "EventDetail"
        var KEY_EVENT_ID = "key_event_id"
    }
}
package com.example.lendahand.adapter;

import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.lendahand.R;
import com.example.lendahand.model.Event;
import com.example.lendahand.util.EventUtil;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.Query;

import me.zhanghai.android.materialratingbar.MaterialRatingBar;

/**
 * RecyclerView adapter for a list of Events.
 */
public class EventAdapter extends FirestoreAdapter<EventAdapter.ViewHolder> {
    private String TAG = "EventAdapter";

    public interface OnEventSelectedListener {
        void onEventSelected(DocumentSnapshot event);
    }

    private OnEventSelectedListener mListener;

    public EventAdapter(Query query, OnEventSelectedListener listener) {
        super(query);
        mListener = listener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        return new ViewHolder(inflater.inflate(R.layout.item_event, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.bind(getSnapshot(position), mListener);
    }


    static class ViewHolder extends RecyclerView.ViewHolder {

        ImageView imageView;
        TextView nameView;
        MaterialRatingBar ratingBar;
        TextView numRatingsView;
        TextView dateView;
        TextView timeView;
        TextView categoryView;
        TextView cityView;

        public ViewHolder(View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.event_item_display);
            nameView = itemView.findViewById(R.id.event_item_name);
            ratingBar = itemView.findViewById(R.id.event_item_rating);
            numRatingsView = itemView.findViewById(R.id.event_item_num_ratings);
            dateView = itemView.findViewById(R.id.event_item_date);
            timeView = itemView.findViewById(R.id.event_item_time);
            categoryView = itemView.findViewById(R.id.event_item_category);
            cityView = itemView.findViewById(R.id.event_item_city);
        }

        public void bind(final DocumentSnapshot snapshot,
                         final OnEventSelectedListener listener) {

            Event event = snapshot.toObject(Event.class);
            Resources resources = itemView.getResources();

            // Load image
            Glide.with(imageView.getContext())
                    .load(event.getPhoto())
                    .into(imageView);

            nameView.setText(event.getName());
            ratingBar.setRating((float) event.getAvgRating());
            cityView.setText(event.getCity());
            categoryView.setText(event.getCategory());
            numRatingsView.setText(resources.getString(R.string.fmt_num_ratings,
                    event.getNumRatings()));
            dateView.setText(EventUtil.Companion.getDateString(event));
            timeView.setText(EventUtil.Companion.getTimeString(event));

            // Click listener
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (listener != null) {
                        listener.onEventSelected(snapshot);
                    }
                }
            });
        }

    }
}

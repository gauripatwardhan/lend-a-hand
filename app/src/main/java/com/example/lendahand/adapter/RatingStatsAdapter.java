package com.example.lendahand.adapter;

import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.lendahand.R;
import com.example.lendahand.model.RatingsStat;
import com.google.firebase.firestore.Query;

import me.zhanghai.android.materialratingbar.MaterialRatingBar;

/**
 * RecyclerView adapter for a bunch of Ratings.
 */
public class RatingStatsAdapter {

    /****************************extends
} FirestoreAdapter<RatingStatsAdapter.ViewHolder> {

    public RatingStatsAdapter(Query query) {
        super(query);
    }

    @NonNull
    @Override
    public RatingStatsAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        return new RatingStatsAdapter.ViewHolder(inflater.inflate(R.layout.item_rating_stats, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.bind(getSnapshot(position).toObject(RatingsStat.class));
    }


    static class ViewHolder extends RecyclerView.ViewHolder {

        MaterialRatingBar ratingBar;
        TextView numRatingsView;

        public ViewHolder(View itemView) {
            super(itemView);
            ratingBar = itemView.findViewById(R.id.event_item_rating1);
            numRatingsView = itemView.findViewById(R.id.event_item_num_ratings1);
        }

        public void bind(RatingsStat stats) {
            ratingBar.setRating((float) stats.getAvgRating());
            Resources resources = itemView.getResources();
            numRatingsView.setText(resources.getString(R.string.fmt_num_ratings,
                    stats.getNumRatings()));
        }
    }
    ****************************/
}


package com.example.lendahand

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import androidx.navigation.fragment.findNavController
import com.example.lendahand.databinding.TitleFragmentBinding
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.storage.FirebaseStorage


/**
 * Contains the opening fragment for when the user is not signed in
 */
class TitleFragment : Fragment() {
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val binding: TitleFragmentBinding = DataBindingUtil.inflate(
            inflater, R.layout.title_fragment, container, false
        )

        binding.signUp.setOnClickListener { view: View ->
            Navigation.findNavController(view).navigate(R.id.action_titleFragment_to_signUpFragment)
        }


        binding.logIn.setOnClickListener { view: View ->
            Navigation.findNavController(view).navigate(R.id.action_titleFragment_to_logInFragment)
        }

        return binding.root
    }

    override fun onStart() {
        super.onStart()
        //  Check if the user is already logged in. If so, just go to EventTrackerFragment
        if (!(!MainActivity.mViewModel!!.getIsSigningIn() &&
                    FirebaseAuth.getInstance().currentUser == null)) {
            Snackbar.make(
                this.activity!!.findViewById(android.R.id.content), "User already logged in",
                Snackbar.LENGTH_SHORT
            ).show()

            var userCur = FirebaseAuth.getInstance().currentUser!!
            FireStoreInstance.userId = userCur.uid

            FireStoreInstance.mStorage = FirebaseStorage.getInstance().getReference()

            val controller = findNavController()
            controller.navigate(R.id.action_titleFragment_to_eventTrackerFragment)
        }
    }
}
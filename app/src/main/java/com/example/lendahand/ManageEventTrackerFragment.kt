package com.example.lendahand

import android.app.Activity
import android.os.Bundle
import android.text.Html
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.core.os.bundleOf
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.navigation.findNavController
import androidx.navigation.fragment.NavHostFragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.lendahand.adapter.EventAdapter
import com.example.lendahand.databinding.ManageEventTrackerBinding
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.firestore.DocumentSnapshot
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.FirebaseFirestoreException
import com.google.firebase.firestore.Query

class ManageEventTrackerFragment : Fragment(), View.OnClickListener,
    FilterDialogFragment.FilterListener, EventAdapter.OnEventSelectedListener,
    MainActivity.OnFilterSelectedListener {

    companion object {
        private val TAG = "ManageEventTracker"

        private val RC_SIGN_IN = 9001

        val LIMIT = 50
    }

    private var mCurrentSearchView: TextView? = null
    private var mCurrentSortByView: TextView? = null
    private var mRestaurantsRecycler: RecyclerView? = null
    private var mEmptyView: ViewGroup? = null

    private var mFirestore: FirebaseFirestore? = null
    private var mQuery: Query? = null

    private var mFilterDialog: FilterDialogFragment? = null
    private var mAdapter: EventAdapter? = null

    private var mActivity: Activity? = null

    private var userId: String? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding: ManageEventTrackerBinding = DataBindingUtil.inflate(
            inflater, R.layout.manage_event_tracker, container, false)

        mActivity = this.activity

        if (Modify.modifiedEvent) {
            Toast.makeText(this.context, "Event changes saved", Toast.LENGTH_LONG).show()
            Modify.modifiedEvent = false
        }

        MainActivity.mCallbackFilter = this

        mCurrentSearchView = binding.textCurrentSearch
        mCurrentSortByView = binding.textCurrentSortBy
        mRestaurantsRecycler = binding.recyclerRestaurants
        mEmptyView = binding.viewEmpty

        binding.filterBar.setOnClickListener(this)
        binding.buttonClearFilter.setOnClickListener(this)
        binding.eventTrackerBackButton.setOnClickListener(this)

        // Enable Firestore logging
        FirebaseFirestore.setLoggingEnabled(true)

        // Initialize Firestore and the main RecyclerView
        initFirestore()
        initRecyclerView()

        // Filter Dialog
        mFilterDialog = FilterDialogFragment()

        // Set user id
        userId = FireStoreInstance.userId

        return binding.root
    }

    private fun initFirestore() {
        mFirestore = FireStoreInstance.mFirestore
        // Get the 50 live, highest rated events that the signed user has created
        mQuery = mFirestore!!.collection("events")
            .orderBy("avgRating", Query.Direction.DESCENDING)
            .whereEqualTo("userId", userId)
            .limit(EventTrackerFragment.LIMIT.toLong())
    }

    private fun initRecyclerView() {
        if (mQuery == null) {
            Log.e(TAG, "No query, not initializing RecyclerView")
        }

        mAdapter = EventAdapter(mQuery, this).apply {

            fun onDataChanged() { // Show/hide content if the query returns empty.
                if (mAdapter!!.getItemCount() === 0) {
                    mRestaurantsRecycler!!.visibility = View.GONE
                    mEmptyView!!.visibility = View.VISIBLE
                } else {
                    mRestaurantsRecycler!!.visibility = View.VISIBLE
                    mEmptyView!!.visibility = View.GONE
                }
            }

            fun onError(e: FirebaseFirestoreException?) { // Show a snackbar on errors
                Snackbar.make(
                    activity!!.findViewById<View>(android.R.id.content),
                    "Error: check logs for info.", Snackbar.LENGTH_LONG
                ).show()
            }
        }
        mRestaurantsRecycler!!.layoutManager = LinearLayoutManager(this.activity)
        mRestaurantsRecycler!!.adapter = mAdapter
    }

    override fun onStart() {
        super.onStart()
        // Apply filters
        onFilter(MainActivity.mViewModel!!.getFilters())
        // Start listening for Firestore updates
        mAdapter?.startListening()
    }

    override fun onStop() {
        super.onStop()
        mAdapter?.stopListening()
    }

    override fun onFilter(filters: Filters?) {
        onFilterSelected(filters)
    }

    override fun onFilterSelected(filters: Filters?) {
        Log.i("EventTracker: onFilter", "Everything works !!!")
        // Construct query basic query
        var query: Query = mFirestore!!.collection("events")

        // Category (equality filter)
        if (filters!!.hasCategory()) {
            query = query.whereEqualTo("category", filters.category)
        }

        // City (equality filter)
        if (filters.hasCity()) {
            query = query.whereEqualTo("city", filters.city)
        }

        // Sort by (orderBy with direction)
        if (filters.hasSortBy()) {
            query = query.orderBy(filters.sortBy, filters.sortDirection)
        }

        // Limit items
        query = query.limit(LIMIT.toLong())

        query = query.whereEqualTo("userId", userId)
        query = query.whereEqualTo("delete", false)

        // Update the query
        mQuery = query
        mAdapter!!.setQuery(query)

        // Set header
        mCurrentSearchView!!.text = Html.fromHtml(filters.getSearchDescription(this.context))
        mCurrentSortByView!!.text = filters.getOrderDescription(this.context)

        // Save filters
        MainActivity.mViewModel!!.setFilters(filters)
    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.filter_bar -> {
                onFilterClicked()
            }
            R.id.button_clear_filter -> {
                onClearFilterClicked()
            }
            R.id.event_tracker_back_button -> {
                onBackArrowClicked(v)
            }
        }
    }

    fun onBackArrowClicked(view: View?) {
        val controller = NavHostFragment.findNavController(this)
        controller.navigate(R.id.action_manageEventTrackerFragment_to_eventTrackerFragment)
    }

    fun onFilterClicked() { // Show the dialog containing filter options
        mFilterDialog!!.show(getActivity()!!.supportFragmentManager, FilterDialogFragment.TAG)
    }

    fun onClearFilterClicked() {
        mFilterDialog!!.resetFilters()
        onFilter(Filters.getDefault())
    }

    override fun onEventSelected(event: DocumentSnapshot) { // Go to the modifiable page for the selected event
        val bundle = bundleOf(ModifyEventDetailFragment.KEY_EVENT_ID to event.id)
        view!!.findNavController().navigate(R.id.action_manageEventTrackerFragment_to_modifyEventDetailFragment, bundle)
    }
}
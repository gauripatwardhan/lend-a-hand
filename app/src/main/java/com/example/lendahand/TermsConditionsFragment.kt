package com.example.lendahand


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.Navigation
import com.example.lendahand.R
import com.example.lendahand.databinding.TermsConditionsFragmentBinding


/**
 * Contains the create Account fragment (allows user to create a new account)
 */

class TermsConditionsFragment : androidx.fragment.app.Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding: TermsConditionsFragmentBinding = DataBindingUtil.inflate(inflater, R.layout.terms_conditions_fragment, container, false)
        return binding.root
    }
}

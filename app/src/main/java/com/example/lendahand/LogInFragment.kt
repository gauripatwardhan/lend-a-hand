package com.example.lendahand

import android.app.Activity
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.example.lendahand.databinding.LogInBinding
import com.google.android.gms.tasks.Task
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.auth.AuthResult
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.storage.FirebaseStorage

class LogInFragment : Fragment() {

    private var mUserNameView: EditText? = null
    private var mPasswordView: EditText? = null
    private lateinit var activityHolder: Activity
    private val TAG = "LogInFragment"

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding: LogInBinding = DataBindingUtil.inflate(
            inflater,
            R.layout.log_in, container, false
        )

        activityHolder = this.activity!!
        mUserNameView = binding.userName
        mPasswordView = binding.password

        binding.createUser.setOnClickListener {
            logInUser();
        }

        binding.backButton.setOnClickListener {
            this.activity!!.onBackPressed()
        }

        return binding.root
    }

    private fun logInUser() {
        val email = (mUserNameView!!.text.toString()).plus("@lendahand.com")
        val password = mPasswordView!!.text.toString()
        FireStoreInstance.mAuth.signInWithEmailAndPassword(email, password)
            .addOnCompleteListener(activityHolder) {task: Task<AuthResult> ->
                if (task.isSuccessful) {
                    Log.d(TAG, "signInWithEmail&Password:success")
                    Snackbar.make(
                        this.activity!!.findViewById(android.R.id.content), "Logged in",
                        Snackbar.LENGTH_SHORT
                    ).show()

                    val controller = findNavController()

                    if (CurrentUserInfo.signInUser) {

                        // Initialize current user
                        var userCur = FirebaseAuth.getInstance().currentUser!!
                        FireStoreInstance.userId = userCur.uid

                        if (CurrentUserInfo.user != null) {
                            CurrentUserInfo.user.setUserId(FireStoreInstance.userId)

                            // Store CurrentUserInfo.user in cloud firestore
                            val users = FireStoreInstance.mFirestore!!.collection("users")
                            // Add the new event events collection
                            users.add(CurrentUserInfo.user)
                                .addOnCompleteListener { task ->
                                    if (task.isSuccessful) {
                                        Log.e(TAG, "User created in personal database")
                                    } else {
                                        Log.e(
                                            TAG,
                                            "User not created in personal database: ".plus(task.exception)
                                        )
                                    }
                                }
                        }

                        // Initiaze firestore storage
                        FireStoreInstance.mStorage = FirebaseStorage.getInstance().getReference()
                    } else {
                        val events = FireStoreInstance.mFirestore!!.collection("events")
                        // Add the new event events collection
                        events.add(CurrentUserInfo.event)

                        CurrentUserInfo.signInUser = true
                    }
                    controller.navigate(R.id.action_logInFragment_to_eventTrackerFragment)
                } else {
                    Log.d(TAG, "signInWithEmail&Password:failure")
                    Log.d(TAG, "Error: ".plus(task.exception))
                    Snackbar.make(
                        this.activity!!.findViewById(android.R.id.content), "Failure. Try Again",
                        Snackbar.LENGTH_SHORT
                    ).show()
                }
            }
    }

}
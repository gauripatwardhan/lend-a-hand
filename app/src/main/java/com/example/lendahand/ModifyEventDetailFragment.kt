package com.example.lendahand

import android.app.Activity
import android.app.DatePickerDialog
import android.app.TimePickerDialog
import android.content.Context
import android.content.Intent
import android.database.Cursor
import android.graphics.Bitmap
import android.media.ThumbnailUtils
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.text.Editable
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import com.bumptech.glide.Glide
import com.example.lendahand.databinding.ModifyEventDetailBinding
import com.example.lendahand.model.Event
import android.text.TextWatcher
import com.example.lendahand.model.RatingsStat
import com.google.firebase.firestore.*
import com.google.firebase.firestore.EventListener
import me.zhanghai.android.materialratingbar.MaterialRatingBar
import java.io.ByteArrayOutputStream
import java.io.FileNotFoundException
import java.io.IOException
import java.io.InputStream
import java.util.*
import java.util.concurrent.TimeUnit

class ModifyEventDetailFragment : Fragment(),
    View.OnClickListener,
    EventListener<DocumentSnapshot>,
    DeleteDialogFragment.DeleteListener,
    MainActivity.OnDeleteSelectedListener {

    private var mImageView: ImageView? = null
    private var mNameView: EditText? = null
    private var mRatingIndicator: MaterialRatingBar? = null
    private var mNumRatingsView: TextView? = null
    private var mCityView: EditText? = null
    private var mStateView: EditText? = null
    private var mZipcodeView: EditText? = null
    private var mCategoryView: Spinner? = null
    private var mDateView: TextView? = null
    private var mTimeView: TextView? = null
    private var mRegistrationView: EditText? = null
    private var mDescriptionView: EditText? = null
    private var mWarningFake: RelativeLayout? = null
    private var mEventRef: DocumentReference? = null
    private var mDeleteDialog: DeleteDialogFragment? = null
    private var mEventRegistration: ListenerRegistration? = null
    private var eventCategorySelected: Any? = null

    private var photoChanged: Boolean = false
    private var videoChanged: Boolean = false
    private var nameChanged: Boolean = false
    private var cityChanged: Boolean = false
    private var descriptionChanged: Boolean = false
    private var stateChanged: Boolean = false
    private var zipcodeChanged: Boolean = false
    private var registrationChanged: Boolean = false
    private var timeChanged: Boolean = false
    private var dateChanged: Boolean = false
    private var categoryChanged: Boolean = false

    private var eventCalnder: Calendar = Calendar.getInstance()

    private var byteArray: ByteArray? = null
    private val SELECT_PICTURE = 0
    private val SELECT_VIDEO = 3


    private lateinit var activityContext: Context

    companion object {
        private const val TAG = "ModifyEventDetail"
        var KEY_EVENT_ID = "key_event_id"
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding: ModifyEventDetailBinding = DataBindingUtil.inflate(
            inflater, R.layout.modify_event_detail, container, false
        )
        activityContext = this.activity!!

        // Get event ID from extras
        val eventId: String = arguments!!.getString(KEY_EVENT_ID)
            ?: throw IllegalArgumentException(("Must pass extra ").plus(KEY_EVENT_ID))
        Log.i(TAG, "Event id received: ".plus(eventId))

        // Get reference to the event
        mEventRef = FireStoreInstance.mFirestore!!.collection("events").document(eventId)

        // Set views
        mWarningFake = binding.warningFake
        mImageView = binding.eventImage
        mNameView = binding.eventName
        mNameView!!.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {}

            override fun afterTextChanged(s: Editable) {
                afterEditTextChanged(s)
            }
        })
        mRatingIndicator = binding.eventRating
        mNumRatingsView = binding.eventNumRatings
        mCityView = binding.eventCity
        mCityView!!.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {}

            override fun afterTextChanged(s: Editable) {
                afterEditTextChanged(s)
            }
        })
        mStateView = binding.eventState
        mStateView!!.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {}

            override fun afterTextChanged(s: Editable) {
                afterEditTextChanged(s)
            }
        })
        mZipcodeView = binding.eventZipcode
        mZipcodeView!!.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {}

            override fun afterTextChanged(s: Editable) {
                afterEditTextChanged(s)
            }
        })
        mTimeView = binding.eventTime
        mDateView = binding.eventDate
        mRegistrationView = binding.registerLink
        mRegistrationView!!.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {}

            override fun afterTextChanged(s: Editable) {
                afterEditTextChanged(s)
            }
        })
        mDescriptionView = binding.description
        mDescriptionView!!.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {}

            override fun afterTextChanged(s: Editable) {
                afterEditTextChanged(s)
            }
        })
        mCategoryView = binding.eventCategory

        // Set listerners
        val mcurrentTime: Calendar = Calendar.getInstance()
        val currentTimeMilli: Long = mcurrentTime.timeInMillis
        mTimeView!!.setOnClickListener {
            val mhour = mcurrentTime.get(Calendar.HOUR_OF_DAY)
            val mminute = mcurrentTime.get(Calendar.MINUTE)
            timeChanged = true

            val mTimePicker = TimePickerDialog(this.context,
                TimePickerDialog.OnTimeSetListener{ timePicker, selectedHour, selectedMinute ->
                    mTimeView!!.text = selectedHour.toString().plus(":").plus(selectedMinute)
                    eventCalnder.set(Calendar.HOUR, selectedHour)
                    if (selectedHour < 12) {
                        eventCalnder.set(Calendar.AM_PM, Calendar.AM)
                    } else {
                        eventCalnder.set(Calendar.AM_PM, Calendar.PM)
                    }
                    eventCalnder.set(Calendar.MINUTE, selectedMinute)
                }, mhour, mminute, true)
            mTimePicker.setTitle("Select Time")
            mTimePicker.show()
        }
        mDateView!!.setOnClickListener {
            val mYear = mcurrentTime.get(Calendar.YEAR);
            val mMonth = mcurrentTime.get(Calendar.MONTH);
            val mDay = mcurrentTime.get(Calendar.DAY_OF_MONTH);
            dateChanged = true

            var mDatePicker = DatePickerDialog(this.context!!, DatePickerDialog.OnDateSetListener()
            { datepicker, selectedyear, selectedmonth, selectedday ->
                mDateView!!.text = (selectedmonth + 1).toString().plus("/")
                    .plus(selectedday).plus("/").plus(selectedyear)
                eventCalnder.set(selectedyear, selectedmonth, (selectedday - 1))
            }, mYear, mMonth, mDay)

            // User can only select dates between today and a year from today
            mDatePicker.datePicker.minDate = currentTimeMilli
            mDatePicker.datePicker.maxDate = currentTimeMilli + TimeUnit.DAYS.toMillis(356)

            mDatePicker.setTitle("Select Date");
            mDatePicker.show();
        }
        binding.eventButtonBack.setOnClickListener(this)
        binding.fabShowDeleteDialog.setOnClickListener(this)
        binding.saveEvent.setOnClickListener { view: View ->
            val eventName: String = mNameView!!.text.toString()
            val description: String = mDescriptionView!!.text.toString()
            var registration: String = mRegistrationView!!.text.toString()
            val city: String = mCityView!!.text.toString()
            val state: String = mStateView!!.text.toString()
            val zip: String =mZipcodeView!!.text.toString()
            var zipcode: Long
            val timestampEvent = eventCalnder.timeInMillis
            binding.error.visibility = View.GONE
            if (eventName.isEmpty()) {
                binding.error.text = "You must provide a name for the event!"
                binding.error.visibility = View.VISIBLE
            } else if (description.isEmpty()) {
                binding.error.text = "You must provide a description!"
                binding.error.visibility = View.VISIBLE
            } else if (eventCategorySelected == null) {
                binding.error.text = "You must pick a category!"
                binding.error.visibility = View.VISIBLE
            } else if (timestampEvent < currentTimeMilli) {
                binding.error.text = "You must provide a proper time for the event!"
                binding.error.visibility = View.VISIBLE
            } else if (city.isEmpty() || state.isEmpty() || zip.isEmpty()) {
                binding.error.text = "You must provide a valid location!"
                binding.error.visibility = View.VISIBLE
            } else if (false && byteArray == null) { // TODO: Remove "false" from the statement
                binding.error.text = "You must upload a photo"
                binding.error.visibility = View.VISIBLE
            } else {
                try {
                    zipcode = zip.toLong()
                } catch(e: Exception) {
                    binding.error.text = "Invalid zipcode. Make sure you only have digits in the zipcode"
                    binding.error.visibility = View.VISIBLE
                    return@setOnClickListener
                }
                if (registration.isEmpty()) {
                    registration = "None"
                }

                // In a transaction, add the new rating and update the aggregate totals
                FireStoreInstance.mFirestore!!.runTransaction { transaction ->
                    val event: Event? = transaction[mEventRef!!]
                        .toObject<Event>(Event::class.java)
                    if (nameChanged) {
                        event!!.setName(eventName)
                    }
                    if (descriptionChanged) {
                        event!!.setDescription(description)
                    }
                    if (cityChanged) {
                        event!!.setCity(city)
                    }
                    if (stateChanged) {
                        event!!.setState(state)
                    }
                    if (zipcodeChanged) {
                        event!!.setZipcode(zipcode)
                    }
                    if (registrationChanged) {
                        event!!.setRegistrationLink(registration)
                    }
                    if (dateChanged || timeChanged) {
                        event!!.setTimeStampEvent(timestampEvent)
                    }
                    if (categoryChanged) {
                        event!!.setCategory(eventCategorySelected!!.toString())
                    }
                    // TODO: Allow user to change video/photo
                    // Commit to Firestore
                    transaction[mEventRef!!] = event!!
                }
                // Navigate to ManageEventTrackerFragment
                Modify.modifiedEvent = true
                Navigation.findNavController(view)
                    .navigate(R.id.action_modifyEventDetailFragment_to_manageEventTrackerFragment)
            }
        }

        // Set the spinner for category selection
        val spinner: Spinner = mCategoryView!!
        ArrayAdapter.createFromResource(
            this.context!!,
            R.array.categories,
            R.layout.spinner_item
        ).also { adapter ->
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            spinner.adapter = adapter
        }
        spinner.onItemSelectedListener = object :
            AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {
                return
            }

            override fun onItemSelected(parent: AdapterView<*>, view: View, pos: Int, id: Long) {
                if (parent.getItemAtPosition(pos).toString() == "Choose a category") { // Do nothing
                } else {
                    // An item was selected. Retrieve the selected item using the position given
                    categoryChanged = true
                    eventCategorySelected = parent.getItemAtPosition(pos)
                }
                Log.e("Category selected", eventCategorySelected.toString())
            }
        }

        // Set OnDeleteSelected
        MainActivity.mCallbackDelete = this

        // Set delete dialog
        mDeleteDialog = DeleteDialogFragment()

        return binding.root
    }

    fun afterEditTextChanged(s: Editable) {
        val v: View = this.activity!!.currentFocus ?: return
        when (v.id) {
            mNameView!!.id -> {
                nameChanged = true
            }
            mDescriptionView!!.id -> {
                descriptionChanged = true
            }
            mRegistrationView!!.id -> {
                registrationChanged = true
            }
            mCityView!!.id -> {
                cityChanged = true
            }
            mStateView!!.id -> {
                stateChanged = true
            }
            mZipcodeView!!.id -> {
                zipcodeChanged = true
            }
        }
    }

    override fun onStart() {
        super.onStart()
        mEventRegistration = mEventRef!!.addSnapshotListener(this)
    }

    override fun onStop() {
        super.onStop()
        if (mEventRegistration != null) {
            mEventRegistration!!.remove()
            mEventRegistration = null
        }
    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.event_button_back -> onBackArrowClicked(v)
            R.id.fab_show_delete_dialog -> onDeleteClicked(v)
        }
    }

    /**
     * Listener for the Event document ([.mEventRef]).
     */
    override fun onEvent(
        snapshot: DocumentSnapshot?,
        e: FirebaseFirestoreException?
    ) {
        var eventFound: Boolean = true
        var ratingStatsFound: Boolean = true
        var event: Event? = null
        var ratingsStat: RatingsStat? = null

        if (e != null) {
            Log.w(TAG, "ModifyEvent:onEvent", e)
            return
        }
        try {
            event = snapshot!!.toObject(Event::class.java)
        } catch (e: Exception) {
            Log.e(TAG, "documentStapshot is not of event")
            eventFound = false;
        }
        if(eventFound) {
            onEventLoaded(event)
        }
    }

    private fun getPosition(category: String?): Int {
        if (category == null) {
            return -1;
        }
        when(category) {
            "Fundraiser" -> {
                return 1
            }
            "Social" -> {
                return 2
            }
            "Research" -> {
                return 3
            }
            "Project" -> {
                return 4
            }
            "General" -> {
                return 5
            }
        }
        if (category == "Choose a category") { // TODO: Remove during final cleanup
            return 0;
        }
        assert(false)
        return -1;
    }

    private fun onEventLoaded(event: Event?) {
        mNameView!!.setText(event!!.getName(), TextView.BufferType.EDITABLE)
        mCityView!!.setText(event.getCity(), TextView.BufferType.EDITABLE)
        mCategoryView!!.setSelection(getPosition(event.getCategory()))
        mTimeView!!.setText(event.getTimeString(), TextView.BufferType.EDITABLE)
        mDateView!!.setText(event.getDateString(), TextView.BufferType.EDITABLE)
        mStateView!!.setText(event.getState(), TextView.BufferType.EDITABLE)
        mZipcodeView!!.setText(event.getZipcode().toString(), TextView.BufferType.EDITABLE)
        mDescriptionView!!.setText(event.getDescription(), TextView.BufferType.EDITABLE)
        eventCalnder.timeInMillis = event.getTimeStampEvent()!!
        mRatingIndicator!!.rating = event.getAvgRating().toFloat()
        mNumRatingsView!!.text = getString(R.string.fmt_num_ratings, event.getNumRatings())
        mRegistrationView!!.setText(event.getRegistrationLink(), TextView.BufferType.EDITABLE)
        if (event.getReported()) {
            mWarningFake!!.visibility = View.VISIBLE
        }
        // Background image
        Glide.with(mImageView!!.context)
            .load(event.getPhoto())
            .into(mImageView!!)
    }

    fun onBackArrowClicked(view: View?) {
        this.activity!!.onBackPressed()
    }

    fun onDeleteClicked(view: View?) {
        mDeleteDialog!!.show(this.activity!!.supportFragmentManager, DeleteDialogFragment.TAG)
    }

    override fun onDelete() {
        onDeleteSelected()
    }

    override fun onDeleteSelected() { // In a transaction, set delete flag on event
        Log.e(TAG, "Setting delete to true")
        Modify.modifiedEvent = true
        mEventRef!!.update("delete", true)
    }
}
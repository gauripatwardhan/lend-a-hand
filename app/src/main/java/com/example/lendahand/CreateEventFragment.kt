package com.example.lendahand

import android.app.Activity.RESULT_OK
import android.app.DatePickerDialog
import android.app.TimePickerDialog
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.text.Html
import android.text.Spanned
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.core.os.bundleOf
import androidx.databinding.DataBindingUtil
import androidx.navigation.Navigation
import androidx.navigation.fragment.findNavController
import com.example.lendahand.databinding.CreateEventBinding
import com.example.lendahand.util.EventUtil
import com.google.android.gms.tasks.Task
import com.google.android.gms.tasks.Tasks
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.storage.UploadTask
import kotlinx.coroutines.*
import java.lang.String.format
import java.util.*
import java.util.concurrent.TimeUnit


/**
 * Contains the create Account fragment (allows user to create a new account)
 * TODO:
 *  Store user id when creating the event
 */

open class CreateEventFragment : androidx.fragment.app.Fragment(),
    AdapterView.OnItemSelectedListener {
    private lateinit var activityContext: Context
    private var byteArray: ByteArray? = null
    private val SELECT_PICTURE = 0
    private val SELECT_VIDEO = 3

    private val TAG = "CreateEventFragment"

    private var eventCategorySelected: Any? = null
    private var mErrorPhoneView: TextView? = null
    private var mPhotoView: ImageView? = null

    private var photoUploaded = false
    private var videoUploaded = false
    private var photoUri: Uri? = null
    private var videoUri: Uri? = null
    private var downloadPhotoUrl: Uri? = null

    private var createInProgress = false // Used to prevent user from creating anther event when
                                        // the first one is in progress

    private var eventName: String = ""
    private var description: String = ""
    private var registration: String = ""
    private var city: String = ""
    private var state: String = ""
    private var zip: String = ""
    private var phone: String = ""
    private var zipcode: Long = 0
    private var timestamp_event: Long = 0


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val binding: CreateEventBinding = DataBindingUtil.inflate(inflater, R.layout.create_event,
            container, false)

        var application = requireNotNull(this.activity).application
        activityContext = this.activity!!

        mErrorPhoneView = binding.errorPhone
        mPhotoView = binding.eventImageDisplay

        var s: String = format(context!!.resources.getString(R.string.phone_number_terms))
        binding.phoneWarning.text = getSpannedText(s)

        binding.termsLink.setOnClickListener {view: View ->
            Navigation.findNavController(view)
                .navigate(R.id.action_createEventFragment_to_termsConditionsFragment)
        }

        binding.eventPicture.setOnClickListener {
            selectImage()
            binding.eventPicture.text = "Image uploaded"
        }

        var eventCalnder: Calendar = Calendar.getInstance()
        eventCalnder.set(Calendar.YEAR, 1970)
        val mcurrentTime: Calendar = Calendar.getInstance()
        val currentTimeMilli: Long = mcurrentTime.timeInMillis
        // TODO: REmove the following 3 lines
        eventCalnder.set(2027, 7, 18)
        eventCalnder.set(Calendar.MINUTE, 45)
        eventCalnder.set(Calendar.HOUR, 20)

        binding.timePickerAction.setOnClickListener {
            val mhour = mcurrentTime.get(Calendar.HOUR_OF_DAY)
            val mminute = mcurrentTime.get(Calendar.MINUTE)

            val mTimePicker = TimePickerDialog(this.context,
                TimePickerDialog.OnTimeSetListener{timePicker, selectedHour, selectedMinute ->
                    binding.timePickerAction.text = selectedHour.toString().plus(":").plus(selectedMinute)
                    eventCalnder.set(Calendar.HOUR, selectedHour)
                    if (selectedHour < 12) {
                        eventCalnder.set(Calendar.AM_PM, Calendar.AM)
                    } else {
                        eventCalnder.set(Calendar.AM_PM, Calendar.PM)
                    }
                    eventCalnder.set(Calendar.MINUTE, selectedMinute)
                }, mhour, mminute, true)
            mTimePicker.setTitle("Select Time")
            mTimePicker.show()
        }

        binding.datePickerAction.setOnClickListener {
            val mYear = mcurrentTime.get(Calendar.YEAR);
            val mMonth = mcurrentTime.get(Calendar.MONTH);
            val mDay = mcurrentTime.get(Calendar.DAY_OF_MONTH);

            var mDatePicker = DatePickerDialog(this.context!!, DatePickerDialog.OnDateSetListener()
                { datepicker, selectedyear, selectedmonth, selectedday ->
                    binding.datePickerAction.text = (selectedmonth + 1).toString().plus("/")
                        .plus(selectedday).plus("/").plus(selectedyear)
                    eventCalnder.set(selectedyear, selectedmonth, (selectedday - 1))
                }, mYear, mMonth, mDay)

            // User can only select dates between today and a year from today
            mDatePicker.datePicker.minDate = currentTimeMilli
            mDatePicker.datePicker.maxDate = currentTimeMilli + TimeUnit.DAYS.toMillis(356)

            mDatePicker.setTitle("Select Date");
            mDatePicker.show();
        }

        val spinner: Spinner = binding.eventCategory
        ArrayAdapter.createFromResource(
            this.context!!,
            R.array.categories,
            android.R.layout.simple_spinner_item
        ).also { adapter ->
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            spinner.adapter = adapter
        }
        spinner.onItemSelectedListener = this

        binding.createEvent.setOnClickListener { view: View ->
            if (createInProgress) {
                Snackbar.make(
                    this.activity!!.findViewById(android.R.id.content), "Creation in progress",
                    Snackbar.LENGTH_SHORT
                ).show()
                return@setOnClickListener
            }
            eventName = binding.eventName.text.toString()
            description = binding.description.text.toString()
            registration = binding.register.text.toString()
            city = binding.city.text.toString()
            state = binding.state.text.toString()
            zip = binding.zip.text.toString()
            phone = binding.phone.text.toString()
            zipcode = 0
            timestamp_event = eventCalnder.timeInMillis
            if (eventName.isEmpty()) {
                binding.errorEventName.text = "You must provide a name for the event!"
                binding.errorEventName.visibility = View.VISIBLE
            } else if (description.isEmpty()) {
                binding.errorDescription.text = "You must provide a description!"
                binding.errorDescription.visibility = View.VISIBLE
            } else if (eventCategorySelected == null) {
                binding.errorCategory.text = "You must pick a category!"
                binding.errorCategory.visibility = View.VISIBLE
            } else if (timestamp_event < currentTimeMilli) {
                binding.errorTime.text = "You must provide a proper time for the event!"
                binding.errorTime.visibility = View.VISIBLE
            } else if (city.isEmpty() || state.isEmpty() || zip.isEmpty()) {
                binding.errorLocation.text = "You must provide a valid location!"
                binding.errorLocation.visibility = View.VISIBLE
            } else if (!photoUploaded) {
                binding.errorPhoto.text = "You must upload a photo"
                binding.errorPhoto.visibility = View.VISIBLE
            } else if (phone.isEmpty() || phone.trim().length < 10) {
                binding.errorPhone.text = "You must provide a valid phone number!"
                binding.errorPhone.visibility = View.VISIBLE
            }  else if (!binding.terms.isChecked) {
                binding.errorTerms.text = "You must check this box!"
                binding.errorTerms.visibility = View.VISIBLE
            } else {
                try {
                    zipcode = zip.toLong()
                } catch(e: Exception) {
                    binding.errorLocation.text = "Invalid zipcode. Make sure you only have digits in the zipcode"
                    binding.errorLocation.visibility = View.VISIBLE
                    return@setOnClickListener
                }
                if (binding.register.text.toString().isEmpty()) {
                    registration = "None"
                }

                createInProgress = true
                binding.progressBar.visibility = View.VISIBLE

                // Upload the photo file and create the event in firebase
                uploadPhoto()
            }
        }

        return binding.root
    }

    private fun getSpannedText(text: String): Spanned {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            return Html.fromHtml(text, Html.FROM_HTML_MODE_COMPACT);
        } else {
            return Html.fromHtml(text);
        }
    }

    private fun randomWord(): String {
        // Create a random word (for file name) that is 10 letters long
        var name = ""
        val random = Random()
        random.nextInt(100)

        for (i in 0 until 10) {
            name += (random.nextInt(100) % 26 + 'a'.toInt()).toString()
        }

        return name
    }

    private fun uploadPhoto() {
        val name = randomWord()
        val photoRef = FireStoreInstance.mStorage.child ("images/".plus(name).plus(".jpg"))
        var setUri = false
        Log.d(TAG, "created photoRef object successfully")

        val uploadTask = photoRef.putFile(photoUri!!)

        uploadTask.continueWithTask { task ->
            if (!task.isSuccessful) {
                task.exception?.let {
                    throw it
                }
            }
            photoRef.downloadUrl
        }.addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    setUri = true
                    downloadPhotoUrl = task.result

                    Log.d(TAG, "Uploaded image successfully: ".plus(downloadPhotoUrl))

                    CurrentUserInfo.event = EventUtil.createEvent(
                        this.context!!,
                        eventName,
                        description,
                        registration,
                        timestamp_event,
                        phone,
                        city,
                        state,
                        zipcode,
                        eventCategorySelected.toString(),
                        FireStoreInstance.userId,
                        FireStoreInstance.mAuth.currentUser!!.displayName!!,
                        downloadPhotoUrl.toString()
                    )

                    CurrentUserInfo.signInUser = false
                    // Call VerifyPhoneFragment to verify the user's phone number
                    val bundle = bundleOf("phonenumber" to phone)
                    val controller = findNavController()
                    controller.navigate(
                        R.id.action_createEventFragment_to_verifyPhoneFragment,
                        bundle
                    )
                } else {
                    setUri = true
                    Log.e(TAG, "Cannot upload image : Can't fetch error")
                    createInProgress = false
                }
            }

    }

    private fun selectImage() {
        val intent = Intent()
        intent.type = "image/*"
        intent.action = Intent.ACTION_GET_CONTENT
        Log.d(TAG, "Opening gallery")
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), SELECT_PICTURE)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        Log.d(TAG, "OnActivitResult is called")

        if (requestCode == SELECT_PICTURE && resultCode == RESULT_OK) {
            if (data?.getData() == null) {
                Log.e(TAG, "onActivityResult for image: data is null")
                mErrorPhoneView!!.text = "You must upload a photo"
                mErrorPhoneView!!.visibility = View.VISIBLE
                photoUploaded = false
                return
            }
            Log.d(TAG, "Photo imported!")
            photoUploaded = true
            photoUri = data.getData()

            try {
                var bitmap = MediaStore.Images.Media.getBitmap(
                    activityContext.getContentResolver(),
                    photoUri
                )
                mPhotoView!!.setImageBitmap(bitmap)
            }
            catch(e: Exception) {
                e.printStackTrace()
            }

        }
    }

    override fun onNothingSelected(parent: AdapterView<*>?) {
        return
    }

    override fun onItemSelected(parent: AdapterView<*>, view: View, pos: Int, id: Long) {
        if (parent.getItemAtPosition(pos).toString() == "Choose a category") { // Do nothing
        } else {
            // An item was selected. Retrieve the selected item using the position given
            eventCategorySelected = parent.getItemAtPosition(pos)
        }
        Log.e("Category selected", eventCategorySelected.toString())
    }
}

package com.example.lendahand.model

class User {
    //TODO: In the final version, there is no need to store username and password since firebase will
    // TODO: store both of them in auth
    private var userId: String? = null
    private var name: String? = null
    private var username: String? = null
    private var password: String? = null
    private var age: Int = -1
    private var city: String? = null
    private var state: String? = null
    private var zipcode: Long = -1

    constructor(name: String, userName: String, password: String, age: Int, city: String, state: String,
                zipcode: Long) {
        this.name = name
        this.username = userName
        this.password = password
        this.age = age
        this.city = city
        this.state = state
        this.zipcode = zipcode
    }

    fun getUserId(): String? {
        return userId
    }

    fun setUserId(userId: String?) {
        this.userId = userId
    }

    fun getUserName(): String? {
        return username
    }

    fun setUserName(username: String?) {
        this.username = username
    }

    fun getName(): String? {
        return name
    }

    fun setName(name: String?) {
        this.name = name
    }

    fun getPassword(): String? {
        return password
    }

    fun setPassword(password: String?) {
        this.password = password
    }

    fun getCity(): String? {
        return city
    }

    fun setCity(city: String?) {
        this.city = city
    }

    fun getState(): String? {
        return state
    }

    fun setState(state: String?) {
        this.state = state
    }

    fun getZipcode(): Long {
        return zipcode
    }

    fun setZipcode(zipcode: Long) {
        this.zipcode = zipcode
    }

    fun getAge(): Int {
        return age
    }

    fun setAge(age: Int) {
        this.age = age
    }

}
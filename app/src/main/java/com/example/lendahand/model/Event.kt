package com.example.lendahand.model

import android.util.Log
import java.sql.Date
import java.sql.Timestamp
import java.text.FieldPosition
import java.text.NumberFormat
import java.text.SimpleDateFormat
import java.time.Instant
import java.time.ZoneId
import java.util.*

/**
 * Event POJO.
 */
class Event {
    companion object {
        val FIELD_CITY = "city"
        val FIELD_CATEGORY = "category"
        val FIELD_DATE = "timeStampEvent"
        val FIELD_POPULARITY = "numRatings"
        val FIELD_AVG_RATING = "avgRating"
    }

    private var name: String? = null
    private var city: String? = null
    private var category: String? = null
    private var photo: String? = null
    private var numRatings = 0
    private var avgRating = 0.0
    private var user_id_id: String? = null
    private var description: String? = null
    private var registrationLink: String? = null
    private var timestamp_event: Long? = null
    private var state: String? = null
    private var zipcode: Long = -1
    private var phone: String? = null
    private var userName: String? = null
    private var delete: Boolean = false
    private var reported: Boolean = false

    constructor() {
        // Default values to help create from firebase query
        this.timestamp_event = 0L
        this.user_id_id = ""
        this.delete = false
    }

    constructor(
        username: String?, name: String?, city: String?, category: String?, photo: String?, numRatings: Int,
        avgRating: Double, user_id: String, description:String?, registrationLink: String?,
        timestamp: Long, state: String?, zipcode: Long, phone: String
    ) {
        this.userName = username
        this.name = name
        this.city = city
        this.category = category
        this.photo = photo
        this.numRatings = numRatings
        this.avgRating = avgRating
        this.user_id_id = user_id
        this.description = description
        this.registrationLink = registrationLink
        this.timestamp_event= timestamp
        this.state = state
        this.zipcode = zipcode
        this.phone = phone
        this.delete = false
    }

    fun getUserName(): String? {
        return userName
    }

    fun setUserName(username: String?) {
        this.userName = username
    }

    fun getName(): String? {
        return name
    }

    fun setName(name: String?) {
        this.name = name
    }

    fun getCity(): String? {
        return city
    }

    fun setCity(city: String?) {
        this.city = city
    }

    fun getCategory(): String? {
        return category
    }

    fun setCategory(category: String?) {
        this.category = category
    }

    fun getPhoto(): String? {
        return photo
    }

    fun setPhoto(photo: String?) {
        this.photo = photo
    }

    fun getNumRatings(): Int {
        return numRatings
    }

    fun setNumRatings(numRatings: Int) {
        this.numRatings = numRatings
    }

    fun getAvgRating(): Double {
        return avgRating
    }

    fun setAvgRating(avgRating: Double) {
        this.avgRating = avgRating
    }

    fun getUserId(): String? {
        return user_id_id
    }

    fun setUserId(user_id: String) {
        this.user_id_id = user_id
    }

    fun getDescription(): String? {
        return description
    }

    fun setDescription(description: String?) {
        this.description = description
    }

    fun getRegistrationLink(): String? {
        return registrationLink
    }

    fun setRegistrationLink(registrationLink: String?) {
        this.registrationLink = registrationLink
    }

    fun getDateString(): String? {
        if (timestamp_event=== null) {
            print("Event:getDate: timestamp_eventis NULL!!!")
            return ""
        } else if (timestamp_event!!.equals(Timestamp(0))) {
            print("Event:getDate: timestamp_eventhas default values")

        }
        return convertDate(timestamp_event!!, "MM/dd/yyyy")
    }

    fun getTimeStampEvent(): Long? {
        if (timestamp_event=== null) {
            return null
        }
        return timestamp_event!!
    }

    private fun convertDate(
        dateInMilliseconds: Long,
        dateFormat: String?
    ): String? {
        var date = Date(dateInMilliseconds)
        val format = SimpleDateFormat(dateFormat, Locale.US)
        format.setTimeZone(TimeZone.getDefault())
        var dateStr = StringBuffer("");
        var fieldPos = FieldPosition(NumberFormat.INTEGER_FIELD)
        fieldPos.setBeginIndex(0)
        dateStr = format.format(date, dateStr, fieldPos)
        return dateStr.toString()
    }

    fun getTimeString(): String? {
        return convertDate(timestamp_event!!, "HH:mm a")
    }

    fun setTimeStampEvent(time: Long) {
        this.timestamp_event= time
    }

    fun setTime(time: Long) {
        // Assuming setDate has just been called before and it has NOT set the time
        this.timestamp_event?.plus(time)
    }

    fun setDate(time: Long) {
        this.timestamp_event?.plus(time)
    }

    fun getState(): String? {
        return state
    }

    fun setState(state: String?) {
        this.state = state
    }

    fun getZipcode(): Long {
        return zipcode
    }

    fun setZipcode(zipcode: Long) {
        this.zipcode = zipcode
    }

    fun getPhone(): String {
        return phone!!
    }

    fun setPhone(phone: String) {
        this.phone = phone
    }

    fun getDelete(): Boolean {
        return delete
    }

    fun setDelete() {
        this.delete = true
    }

    fun setReported() {
        this.reported = true
    }

    fun getReported(): Boolean {
        return reported
    }
}

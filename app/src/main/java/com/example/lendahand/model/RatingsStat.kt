package com.example.lendahand.model

class RatingsStat {
    /****************************
    private var avgRating = 0.0
    private var numRatings = 0

    constructor() {
        this.avgRating = 0.0
        this.numRatings = 0
    }

    constructor(avgRating: Double, numRatings: Int) {
        this.avgRating = avgRating
        this.numRatings = numRatings
    }

    fun getAvgRating(): Double {
        return avgRating
    }

    fun setAvgRating(avgRating: Double) {
        this.avgRating = avgRating
    }

    fun getNumRatings(): Int {
        return numRatings
    }

    fun setNumRatings(numRatings: Int) {
        this.numRatings = numRatings
    }
    ****************************/
}
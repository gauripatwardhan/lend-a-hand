package com.example.lendahand

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.text.Html
import android.util.Log
import android.view.*
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.view.menu.MenuBuilder
import androidx.appcompat.widget.Toolbar
import androidx.core.os.bundleOf
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.lendahand.adapter.EventAdapter
import com.example.lendahand.databinding.EventTrackerBinding
import com.example.lendahand.model.Event
import com.example.lendahand.util.EventUtil
import com.firebase.ui.auth.AuthUI
import com.firebase.ui.auth.AuthUI.IdpConfig.EmailBuilder
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.DocumentSnapshot
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.FirebaseFirestoreException
import com.google.firebase.firestore.Query
import com.google.firebase.functions.FirebaseFunctions

class EventTrackerFragment : Fragment(), View.OnClickListener,
    FilterDialogFragment.FilterListener, EventAdapter.OnEventSelectedListener,
    MainActivity.OnFilterSelectedListener, DeleteDialogAccountFragment.DeleteAccountListener,
    MainActivity.OnDeleteAccountSelectedListener {

    companion object {
        private val TAG = "EventTracker"

        private val RC_SIGN_IN = 9001

        val LIMIT = 50

        private val BACK_STACK_ROOT_TAG = "root_fragment"
    }

    private var mToolbar: Toolbar? = null
    private var mCurrentSearchView: TextView? = null
    private var mCurrentSortByView: TextView? = null
    private var mRestaurantsRecycler: RecyclerView? = null
    private var mEmptyView: ViewGroup? = null
    private var mDeleteDialog: DeleteDialogAccountFragment? = null

    private var mFirestore: FirebaseFirestore? = null
    private var mQuery: Query? = null

    private var mFilterDialog: FilterDialogFragment? = null
    private var mAdapter: EventAdapter? = null

    private var activityHolder: Activity? = null

    private lateinit var functions: FirebaseFunctions

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding: EventTrackerBinding = DataBindingUtil.inflate(
            inflater, R.layout.event_tracker, container, false)

        activityHolder = this.activity

        MainActivity.mCallbackFilter = this

        mToolbar = binding.toolbar
        (getActivity() as AppCompatActivity).setSupportActionBar(mToolbar)

        setHasOptionsMenu(true)

        mCurrentSearchView = binding.textCurrentSearch
        mCurrentSortByView = binding.textCurrentSortBy
        mRestaurantsRecycler = binding.recyclerRestaurants
        mEmptyView = binding.viewEmpty

        binding.filterBar.setOnClickListener(this)
        binding.buttonClearFilter.setOnClickListener(this)

        // Enable Firestore logging
        FirebaseFirestore.setLoggingEnabled(true)

        // Initialize Firestore and the main RecyclerView
        initFirestore()
        initRecyclerView()

        // Filter Dialog
        mFilterDialog = FilterDialogFragment()

        // Set OnDeleteSelected
        MainActivity.mCallbackDeleteAccount = this

        // Set delete dialog
        mDeleteDialog = DeleteDialogAccountFragment()

        return binding.root
    }

    private fun initFirestore() {
        mFirestore = FireStoreInstance.mFirestore
        // Get the 50 highest rated restaurants
        mQuery = mFirestore!!.collection("events")
            .orderBy("avgRating", Query.Direction.DESCENDING)
            .whereEqualTo("delete", false)
            .limit(LIMIT.toLong())
    }

    private fun initRecyclerView() {
        if (mQuery == null) {
            Log.w(TAG, "No query, not initializing RecyclerView")
        }

        mAdapter = EventAdapter(mQuery, this).apply {

            fun onDataChanged() { // Show/hide content if the query returns empty.
                if (mAdapter!!.getItemCount() === 0) {
                    mRestaurantsRecycler!!.visibility = View.GONE
                    mEmptyView!!.visibility = View.VISIBLE
                } else {
                    mRestaurantsRecycler!!.visibility = View.VISIBLE
                    mEmptyView!!.visibility = View.GONE
                }
            }

            fun onError(e: FirebaseFirestoreException?) { // Show a snackbar on errors
                Snackbar.make(
                    activityHolder!!.findViewById<View>(android.R.id.content),
                    "Error: check logs for info.", Snackbar.LENGTH_LONG
                ).show()
            }
        }

        mRestaurantsRecycler!!.layoutManager = LinearLayoutManager(activityHolder)
        mRestaurantsRecycler!!.adapter = mAdapter

    }

    override fun onStart() {
        super.onStart()
        // Apply filters
        onFilter(MainActivity.mViewModel!!.getFilters())
        // Start listening for Firestore updates
        mAdapter?.startListening()
    }

    override fun onStop() {
        super.onStop()
        mAdapter?.stopListening()
    }

    private fun onAddItemsClicked() { // Get a reference to the events collection
        val restaurants = mFirestore!!.collection("events")
        for (i in 0..9) { // Get a random Restaurant POJO
            val restaurant: Event = EventUtil.getRandom(this.context!!)
            // Add a new document to the restaurants collection
            restaurants.add(restaurant)
        }
    }

    override fun onFilter(filters: Filters?) {
        onFilterSelected(filters)
    }

    override fun onFilterSelected(filters: Filters?) {
        Log.i("EventTracker: onFilter", "Everything works !!!")
        // Construct query basic query
        var query: Query = mFirestore!!.collection("events")

        // Category (equality filter)
        if (filters!!.hasCategory()) {
            query = query.whereEqualTo("category", filters.category)
        }

        // City (equality filter)
        if (filters.hasCity()) {
            query = query.whereEqualTo("city", filters.city)
        }

        // Sort by (orderBy with direction)
        if (filters.hasSortBy()) {
            Log.e("onFiler", "Selected ".plus(filters.sortBy))
            Log.e("onFiler", "Selected direction ".plus(filters.sortDirection))
            query = query.orderBy(filters.sortBy, filters.sortDirection)
        }

        // Limit items
        query = query.limit(LIMIT.toLong())

        // Get an object iff delete is not set
        query = query.whereEqualTo("delete", false)

        // Update the query
        mQuery = query
        mAdapter!!.setQuery(query)

        // Set header
        mCurrentSearchView!!.text = Html.fromHtml(filters.getSearchDescription(this.context))
        mCurrentSortByView!!.text = filters.getOrderDescription(this.context)

        // Save filters
        MainActivity.mViewModel!!.setFilters(filters)
    }

    @SuppressLint("RestrictedApi")
    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu_main, menu)

        if (menu is MenuBuilder) {
            menu.setOptionalIconsVisible(true)
        }
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val controller = findNavController()
        when (item.itemId) {
            R.id.create_event -> {
                controller.navigate(R.id.action_eventTrackerFragment_to_createEventFragment)
            }
            R.id.menu_sign_out -> {
                FireStoreInstance.mAuth.signOut()
                controller.navigate(R.id.action_eventTrackerFragment_to_titleFragment)
            }
            R.id.manage_event -> {
                controller.navigate(R.id.action_eventTrackerFragment_to_manageEventTrackerFragment)
            }
            R.id.delete_account -> {
                onDeleteClicked(null)
            }
        }

        return super.onOptionsItemSelected(item)
    }

    override fun onActivityResult(
        requestCode: Int,
        resultCode: Int,
        data: Intent?
    ) {
        super.onActivityResult(requestCode, resultCode, data)
    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.filter_bar -> {
                onFilterClicked()
            }
            R.id.button_clear_filter -> {
                onClearFilterClicked()
            }
        }
    }

    fun onFilterClicked() { // Show the dialog containing filter options
        mFilterDialog!!.show(getActivity()!!.supportFragmentManager, FilterDialogFragment.TAG)
    }

    fun onClearFilterClicked() {
        mFilterDialog!!.resetFilters()
        onFilter(Filters.getDefault())
    }

    override fun onEventSelected(event: DocumentSnapshot) { // Go to the details page for the selected event
        val bundle = bundleOf(EventDetailFragment.KEY_EVENT_ID to event.id)
        view!!.findNavController().navigate(R.id.action_eventTrackerFragment_to_eventDetailFragment, bundle)
    }

    fun onDeleteClicked(view: View?) {
        mDeleteDialog!!.show(this.activity!!.supportFragmentManager, DeleteDialogAccountFragment.TAG)
    }

    override fun onDeleteAccount() {
        onDeleteSelected()
    }

    override fun onDeleteSelected() { // In a transaction, set delete flag on event
        FireStoreInstance.mAuth.currentUser!!.delete()
            .addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    Log.d(TAG, "User account deleted.")
                    Snackbar.make(
                        this.activity!!.findViewById(android.R.id.content), "Account deleted",
                        Snackbar.LENGTH_SHORT
                    ).show()
                    val controller = findNavController()
                    controller.navigate(R.id.action_eventTrackerFragment_to_titleFragment)
                } else {
                    Log.d(TAG, "Error: ".plus(task.exception))
                    Snackbar.make(
                        this.activity!!.findViewById(android.R.id.content), "Error: Sign in and try again",
                        Snackbar.LENGTH_SHORT
                    ).show()

                }
            }
    }

    private fun shouldStartSignIn(): Boolean {
        return !MainActivity.mViewModel!!.getIsSigningIn() && FirebaseAuth.getInstance().currentUser == null
    }

    private fun startSignIn() { // Sign in with FirebaseUI
        val intent = AuthUI.getInstance().createSignInIntentBuilder()
            .setAvailableProviders(
                listOf(
                    EmailBuilder().build()
                )
            )
            .setIsSmartLockEnabled(false)
            .build()
        startActivityForResult(intent, RC_SIGN_IN)
        MainActivity.mViewModel!!.setIsSigningIn(true)
    }
}
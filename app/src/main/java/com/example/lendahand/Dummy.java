package com.example.lendahand;

import android.util.Log;

import com.example.lendahand.adapter.EventAdapter;
import com.google.firebase.auth.FirebaseAuth;

public class Dummy /*implements EventAdapter.testInterface*/ {
    public String id = null;
    private static String TAG = "Dummy:DummyMe";
    private static Dummy myself = null;

    public Dummy() {
        id = "10";
        myself = null;
    }
    /*******************
    @Override
    public void testUser(FirebaseAuth auth) {
        if (auth.getCurrentUser() == null) {
            Log.d(TAG, "testUser: auth is null (user not logged)");
        } else {
            Log.d(TAG, "testUser: auth is NOT null (user logged)");
        }
        Log.d(TAG, "testUser: Trying to check user login status on "+(System.identityHashCode(FirebaseAuth.getInstance())));
        if (FirebaseAuth.getInstance().getCurrentUser() == null) {
            Log.d(TAG, "testUser: User is NOT logged in. Returning to called onEvent");
            return;
        }
        Log.d(TAG, "testUser: Confirmed that user is logged in"+(System.identityHashCode(FirebaseAuth.getInstance())));
    }
    ***********************/

    public String getId() { return id;
    }

    public static Dummy getInstance() {
        if (myself == null) {
            myself = new Dummy();
            return myself;
        }
        return myself;
    }

    public void dummyMe(FirebaseAuth auth) {
        // TODO: Make sure the user is signed in
        if (auth.getCurrentUser() == null) {
            Log.d(TAG, "dummyMe: auth is null (user not logged)");
        } else {
            Log.d(TAG, "dummyMe: auth is NOT null (user logged)");
        }
        Log.d(TAG, "dummyMe: Trying to check user login status on "+(System.identityHashCode(FirebaseAuth.getInstance())));
        if (FirebaseAuth.getInstance().getCurrentUser() == null) {
            Log.d(TAG, "dummyMe: User is NOT logged in. Returning to called onEvent");
            return;
        }
        Log.d(TAG, "dummyMe: Confirmed that user is logged in"+(System.identityHashCode(FirebaseAuth.getInstance())));

    }
}

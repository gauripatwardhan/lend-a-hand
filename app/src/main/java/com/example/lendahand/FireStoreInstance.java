package com.example.lendahand;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.storage.StorageReference;

public class FireStoreInstance {
    public static FirebaseFirestore mFirestore = null;
    public static String userId = null;
    public static FirebaseAuth mAuth = null;
    public static StorageReference mStorage = null;

}
package com.example.lendahand

import android.os.Build
import android.os.Bundle
import android.text.Html
import android.text.Spanned
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.databinding.DataBindingUtil
import androidx.navigation.Navigation
import com.example.lendahand.databinding.SignUpBinding
import com.example.lendahand.model.User
import com.google.firebase.FirebaseException
import com.google.firebase.FirebaseTooManyRequestsException
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException
import com.google.firebase.auth.PhoneAuthCredential
import com.google.firebase.auth.PhoneAuthProvider
import java.util.concurrent.TimeUnit


/**
 * Contains the create Account fragment (allows user to create a new account)
 */

class SignUpFragment : androidx.fragment.app.Fragment() {

    private val TAG = "SignUpFragment"

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val binding: SignUpBinding = DataBindingUtil.inflate(inflater, R.layout.sign_up, container, false)

        var application = requireNotNull(this.activity).application

        binding.termsLink.setOnClickListener {view: View ->
            Navigation.findNavController(view)
                .navigate(R.id.action_signUpFragment_to_termsConditionsFragment)
        }

        binding.backButton.setOnClickListener {
            this.activity!!.onBackPressed()
        }

        var s: String =
            java.lang.String.format(context!!.resources.getString(R.string.phone_number_terms))
        binding.phoneWarning.text = getSpannedText(s)

        binding.createUser.setOnClickListener { view: View ->
            val name: String = binding.name.text.toString()
            val user_name: String = binding.username.text.toString()
            val password: String = binding.password.text.toString()
            val city: String = binding.city.text.toString()
            val state: String = binding.state.text.toString()
            val zip: String = binding.zip.text.toString()
            val phone: String = binding.phone.text.toString()
            val age: String = binding.age.text.toString()
            val zipcode: Long
            val ageNumber: Int
            if (name.isEmpty()) {
                binding.errorName.text = "You must enter a name!"
                binding.errorName.visibility = View.VISIBLE
            } else if (user_name.isEmpty()) {
                binding.errorUserName.text = "You must provide a user name!"
                binding.errorUserName.visibility = View.VISIBLE
            } else if (password.isEmpty()) {
                binding.errorPassword.text = "You must provide a password!"
                binding.errorPassword.visibility = View.VISIBLE
            } else if (password.trim().length < 6) {
                binding.errorPassword.text = "Password should be at least 6 characters long"
                binding.errorPassword.visibility = View.VISIBLE
            } else if (city.isEmpty() || state.isEmpty() || zip.isEmpty()) {
                binding.errorLocation.text = "You must provide a valid location!"
                binding.errorLocation.visibility = View.VISIBLE
            } else if (age.isEmpty()) {
                binding.errorAge.text = "You must provide an age!"
                binding.errorAge.visibility = View.VISIBLE
            } else if (phone.isEmpty() || phone.trim().length < 10) {
                binding.errorPhone.text = "You must provide a valid phone number!"
                binding.errorPhone.visibility = View.VISIBLE
            } else if (!binding.terms.isChecked) {
                binding.errorTerms.text = "You must check this box!"
                binding.errorTerms.visibility = View.VISIBLE
            } else {
                try {
                    zipcode = zip.toLong()
                } catch(e: Exception) {
                    binding.errorLocation.text = "Invalid zipcode. Make sure you only have digits in the zipcode"
                    binding.errorLocation.visibility = View.VISIBLE
                    return@setOnClickListener
                }
                try {
                    ageNumber = age.toInt()
                } catch(e: Exception) {
                    binding.errorAge.text = "Invalid age. Make sure you only have digits."
                    binding.errorAge.visibility = View.VISIBLE
                    return@setOnClickListener
                }

                if (ageNumber < 18) {
                    binding.errorAge.text = "You are underage!"
                    binding.errorAge.visibility = View.VISIBLE
                    return@setOnClickListener
                }

                if (ageNumber > 118) {
                    binding.errorAge.text = "Invalid age!"
                    binding.errorAge.visibility = View.VISIBLE
                    return@setOnClickListener
                }

                // Store user information in CurrentUserInfo.user
                CurrentUserInfo.user = User(name, user_name, password, ageNumber,
                    city, state, zipcode)
                CurrentUserInfo.setUserResquest(user_name)

                // Call VerifyPhoneFragment to verify the user's phone number
                val bundle = bundleOf("phonenumber" to phone)
                Navigation.findNavController(view)
                    .navigate(R.id.action_signUpFragment_to_verifyPhoneFragment, bundle)
            }
        }
        return binding.root
    }

    private fun getSpannedText(text: String): Spanned {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            return Html.fromHtml(text, Html.FROM_HTML_MODE_COMPACT);
        } else {
            return Html.fromHtml(text);
        }
    }
}

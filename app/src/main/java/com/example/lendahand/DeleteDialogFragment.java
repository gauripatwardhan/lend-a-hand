package com.example.lendahand;

import android.content.Context; 
import android.os.Bundle; 
import android.util.Log; 
import android.view.LayoutInflater; 
import android.view.View; 
import android.view.ViewGroup; 
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable; 
import androidx.fragment.app.DialogFragment;
import com.google.firebase.auth.FirebaseAuth;

/**
 * Dialog Fragment containing delete form.
 */
public class DeleteDialogFragment extends DialogFragment implements View.OnClickListener {

    public static final String TAG = "DeleteDialog";

    interface DeleteListener {
        void onDelete();
    }

    private DeleteListener mDeleteListener;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.dialog_delete_event, container, false);

        v.findViewById(R.id.event_form_delete).setOnClickListener(this);
        v.findViewById(R.id.event_form_cancel).setOnClickListener(this);

        return v;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        if (context instanceof DeleteListener) {
            mDeleteListener = (DeleteListener) context;
        } else {
            Log.e(TAG, "onAttach: context does not implement onDelete() (interface issue)");
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        getDialog().getWindow().setLayout(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.event_form_delete:
                onDeleteClicked(v);
                break;
            case R.id.event_form_cancel:
                onCancelClicked(v);
                break;
        }
    }

    public void onDeleteClicked(View view) {
        if (mDeleteListener != null) {
            mDeleteListener.onDelete();
        } else {
            Log.e(TAG, "deleteClicked and mDeleteListener is null");
        }

        dismiss();
    }

    public void onCancelClicked(View view) {
        dismiss();
    }
}

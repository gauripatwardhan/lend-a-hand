package com.example.lendahand

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.ViewModelProvider
import com.example.lendahand.databinding.ActivityMainBinding
import com.example.lendahand.model.Rating
import com.example.lendahand.viewmodel.UserSignInStatusViewModel
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore


class MainActivity : AppCompatActivity(), FilterDialogFragment.FilterListener,
    RatingDialogFragment.RatingListener, DeleteDialogFragment.DeleteListener,
    DeleteDialogAccountFragment.DeleteAccountListener{

    private lateinit var binding: ActivityMainBinding

    interface OnFilterSelectedListener {
        fun onFilterSelected(filters: Filters?)
    }

    interface OnRatingSelectedListener {
        fun onRatingSelected(rating: Rating)
    }

    interface OnDeleteSelectedListener {
        fun onDeleteSelected()
    }


    interface OnDeleteAccountSelectedListener {
        fun onDeleteSelected()
    }

    companion object {
        var mCallbackFilter: OnFilterSelectedListener? = null
        var mCallbackRating: OnRatingSelectedListener? = null
        var mCallbackDelete: OnDeleteSelectedListener? = null
        var mCallbackDeleteAccount: OnDeleteAccountSelectedListener? = null
        // View model
        var mViewModel: UserSignInStatusViewModel? = null
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        val view = binding.root

        initFirestore()
        mViewModel =
            ViewModelProvider(this).get(UserSignInStatusViewModel::class.java)

        FireStoreInstance.mAuth = FirebaseAuth.getInstance()

        setContentView(view)
    }

    private fun initFirestore() {
        FireStoreInstance.mFirestore = FirebaseFirestore.getInstance()
    }

    override fun onFilter(filter: Filters?) {
        mCallbackFilter!!.onFilterSelected(filter);
    }

    override fun onRating(rating: Rating) {
        mCallbackRating!!.onRatingSelected(rating)
    }

    override fun onDelete() {
        mCallbackDelete!!.onDeleteSelected()
    }

    override fun onDeleteAccount() {
        mCallbackDeleteAccount!!.onDeleteSelected()
    }
}
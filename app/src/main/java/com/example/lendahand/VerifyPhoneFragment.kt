package com.example.lendahand

import android.app.Activity
import android.os.Build
import android.os.Bundle
import android.text.Html
import android.text.Spanned
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.navigation.fragment.findNavController
import com.example.lendahand.databinding.PhoneValidationBinding
import com.google.android.gms.tasks.Task
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.FirebaseException
import com.google.firebase.FirebaseTooManyRequestsException
import com.google.firebase.auth.*
import java.util.concurrent.TimeUnit

class VerifyPhoneFragment : Fragment() {
    private val TAG = "VerifyPhoneFragment"
    private val BACK_STACK_ROOT_TAG = "root_fragment"
    private var verificationInProgress = false
    private var storedVerificationId: String? = ""
    private lateinit var resendToken: PhoneAuthProvider.ForceResendingToken
    private lateinit var callbacks: PhoneAuthProvider.OnVerificationStateChangedCallbacks
    private lateinit var activityHolder: Activity
    private lateinit var phone: String
    private lateinit var userCode: String


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val binding: PhoneValidationBinding = DataBindingUtil.inflate(
            inflater,
            R.layout.phone_validation, container, false
        )

        activityHolder = this.activity!!

        // For testing send phone number as "+16505551234" or "+16505551244"
        phone = arguments!!.getString("phonenumber")
            ?: throw IllegalArgumentException(("Must pass extra phonenumber"))

        val smsCode = "123456"

        val firebaseAuthSettings = FireStoreInstance.mAuth!!.firebaseAuthSettings

        // Configure faking the auto-retrieval with the whitelisted numbers.
        firebaseAuthSettings.setAutoRetrievedSmsCodeForPhoneNumber(phone, smsCode)

        val callbacks = object : PhoneAuthProvider.OnVerificationStateChangedCallbacks() {

            override fun onVerificationCompleted(credential: PhoneAuthCredential) {
                // This callback will be invoked in two situations:
                // 1 - Instant verification. In some cases the phone number can be instantly
                //     verified without needing to send or enter a verification code.
                // 2 - Auto-retrieval. On some devices Google Play services can automatically
                //     detect the incoming verification SMS and perform verification without
                //     user action.
                Log.d(TAG, "onVerificationCompleted:$credential")
                signInWithPhoneAuthCredential(credential)
                Log.d(TAG, "signed in user via auto verify")
            }

            override fun onVerificationFailed(e: FirebaseException) {
                // This callback is invoked in an invalid request for verification is made,
                // for instance if the the phone number format is not valid.
                Log.w(TAG, "onVerificationFailed", e)

                if (e is FirebaseAuthInvalidCredentialsException) {
                    Snackbar.make(
                        activityHolder!!.findViewById(android.R.id.content), "Invalid phone number!",
                        Snackbar.LENGTH_SHORT
                    ).show()
                } else if (e is FirebaseTooManyRequestsException) {
                    Log.e(TAG, "Too many requests")
                    Snackbar.make(
                        activityHolder!!.findViewById(android.R.id.content), "Internal error. Please try again later",
                        Snackbar.LENGTH_SHORT
                    ).show()
                }

                Snackbar.make(
                    activityHolder!!.findViewById(android.R.id.content), "Could not send text to phone number.",
                    Snackbar.LENGTH_SHORT
                ).show()
            }

            override fun onCodeSent(
                verificationId: String,
                token: PhoneAuthProvider.ForceResendingToken
            ) {
                // The SMS verification code has been sent to the provided phone number, we
                // now need to ask the user to enter the code and then construct a credential
                // by combining the code with a verification ID.
                Log.d(TAG, "onCodeSent:$verificationId")

                // Save verification ID and resending token so we can use them later
                storedVerificationId = verificationId
                resendToken = token
            }
        }


        var s: String =
            java.lang.String.format(context!!.resources.getString(R.string.phone_number_terms))
        binding.phoneWarning.text = getSpannedText(s)

        MainActivity.mViewModel!!.setIsSigningIn(true)

        PhoneAuthProvider.getInstance().verifyPhoneNumber(
            phone, // Phone number to verify
            90, // Timeout duration
            TimeUnit.SECONDS, // Unit of timeout
            this.activity!!, // Activity (for callback binding)
            callbacks) // OnVerificationStateChangedCallbacks

        binding.verityPhoneAction.setOnClickListener {view: View ->
            userCode = binding.userCode.text.toString()
            Log.w("UserCode", "The user entered the code as ".plus(userCode))
            verifyUserCode(userCode)
        }

        return binding.root

    }

    private fun verifyUserCode(userCode: String) {
        // TODO: Test this function rigorously
        var credential: PhoneAuthCredential = PhoneAuthProvider.getCredential(storedVerificationId!!, userCode)
        Log.d(TAG, "verfiingUserCode:$credential")

        signInWithPhoneAuthCredential(credential)

    }

    private fun signInWithPhoneAuthCredential(credential: PhoneAuthCredential) {
        FireStoreInstance.mAuth!!.signInWithCredential(credential)
            .addOnCompleteListener(activityHolder) { task: Task<AuthResult> ->
            if (task.isSuccessful) {
                // Sign in success, update UI with the signed-in user's information
                Log.d(TAG, "signInWithCredential:success")
                Snackbar.make(
                    activityHolder!!.findViewById(android.R.id.content), "Success!!!",
                    Snackbar.LENGTH_SHORT
                ).show()

                MainActivity.mViewModel!!.setIsSigningIn(false)

                if (CurrentUserInfo.signInUser) {

                    var userCur = FirebaseAuth.getInstance().currentUser!!

                    // Set user email - used during log-in
                    userCur.updateEmail(CurrentUserInfo.user.getUserName().plus("@lendahand.com"))
                        .addOnCompleteListener { task ->
                            if (task.isSuccessful) {
                                Log.d(TAG, "User email address updated.")
                            } else {
                                Log.d(TAG, "Cannot update email due to: ".plus(task.exception))
                            }
                        }

                    // Prompt the user to re-provide their sign-in credentials
                    userCur.reauthenticate(credential)
                        .addOnCompleteListener {
                            // Set user's display name
                            userCur.updateProfile(CurrentUserInfo.userRequest)
                                .addOnCompleteListener { task ->
                                    if (task.isSuccessful) {
                                        Log.d(TAG, "User profile updated.")
                                    } else {
                                        Log.d(
                                            TAG,
                                            "Cannot update profile due to: ".plus(task.exception)
                                        )
                                    }
                                }
                        }

                    // Prompt the user to re-provide their sign-in credentials
                    userCur.reauthenticate(credential)
                        .addOnCompleteListener {
                            // Set user password
                            userCur.updatePassword(CurrentUserInfo.user.getPassword()!!)
                                .addOnCompleteListener { task ->
                                    if (task.isSuccessful) {
                                        Log.d(TAG, "User password set.")
                                    } else {
                                        Log.d(
                                            TAG,
                                            "Cannot set password due to: ".plus(task.exception)
                                        )
                                    }
                                }
                        }
                }

                // TODO: Pop off everything up to and including the current tab
                if (FirebaseAuth.getInstance().currentUser == null) {
                    Log.e(TAG, "User NOT logged in. Address: ".plus(System.identityHashCode(FirebaseAuth.getInstance())))
                } else {
                    Log.e(TAG, "User is logged in".plus(System.identityHashCode(FirebaseAuth.getInstance())))
                }
                val controller = findNavController()
                controller.navigate(R.id.action_verifyPhoneFragment_to_logInFragment)
            } else {
                // Sign in failed, display a message and update the UI
                Log.w(TAG, "signInWithCredential:failure", task.exception)
                if (task.exception is FirebaseAuthInvalidCredentialsException) {
                    Snackbar.make(
                        activityHolder!!.findViewById(android.R.id.content), "Invalid code.",
                        Snackbar.LENGTH_SHORT
                    ).show()
                }
                Snackbar.make(
                    activityHolder!!.findViewById(android.R.id.content), "Could not verfity phone number. Try again.",
                    Snackbar.LENGTH_SHORT
                ).show()

                // Need to delete the objects????
                CurrentUserInfo.user = null
                CurrentUserInfo.userRequest = null
                CurrentUserInfo.event = null

                // TODO: Pop off everything up to and including the current tab
                val controller = findNavController()
                if (CurrentUserInfo.signInUser) {
                    controller.navigate(R.id.action_verifyPhoneFragment_to_signUpFragment)
                } else {
                    controller.navigate(R.id.action_verifyPhoneFragment_to_createEventFragment)
                    CurrentUserInfo.signInUser = true
                }
            }
        }
    }

    private fun getSpannedText(text: String): Spanned {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            return Html.fromHtml(text, Html.FROM_HTML_MODE_COMPACT);
        } else {
            return Html.fromHtml(text);
        }
    }
}
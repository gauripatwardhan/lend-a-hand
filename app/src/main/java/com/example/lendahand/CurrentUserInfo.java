package com.example.lendahand;

import android.net.Uri;

import com.example.lendahand.model.Event;
import com.example.lendahand.model.User;
import com.google.firebase.auth.UserProfileChangeRequest;

/**
 * This class is used by SignUpFragment to temporarily store user information.
 * If user's phone number is verified by VerifyPhoneFragment, CurrentUserInfo.user
 * will be stored in Firebase. If the phone number cannot be verified,
 * CurrentUserInfo.user will be made null and the user will have to fill out
 * SignUpFragment all over again.
 */
public class CurrentUserInfo {
    public static UserProfileChangeRequest userRequest = null;
    public static User user = null;
    public static boolean signInUser = true;
    public static boolean verifiedUser = false;
    public static Event event = null;

    public static void setUserResquest(String user_name) {
        userRequest = new UserProfileChangeRequest.Builder()
            .setDisplayName(user_name)
                .setPhotoUri(Uri.parse("https://storage.googleapis.com/firestorequickstarts.appspot.com/food_1.png"))
                .build();
    }
}

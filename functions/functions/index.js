// Firebase App (the core Firebase SDK) is always required and
// must be listed before other Firebase SDKs
var firebase = require("firebase/app");

// Add the Firebase products that you want to use
require("firebase/auth");
require("firebase/firestore");


// Import the Firebase SDK for Google Cloud Functions.
const functions = require('firebase-functions');

// Import and initialize the Firebase Admin SDK.
const admin = require('firebase-admin');
admin.initializeApp();

exports.updateUser = functions.firestore
  .document('events/{eventId}')
  .onUpdate((change, context) => {
  const newValue = change.after.data();

  // ...or the previous value before this update
  const previousValue = change.before.data();

  // access a particular field as you would any JS property
  const name = newValue.city;
  console.log("Event modified. New city " + name);

  const answers = ['ok', ];

  return Promise.resolve((answers));
  // perform desired operations ...
});


// Adds a message that welcomes new users into the chat.
exports.addWelcomeMessages = functions.auth.user().onCreate(async (user) => {
  console.log('A new user signed in for the first time.');
  const fullName = user.displayName || 'Anonymous';

  console.log('Welcome message written to database.');
});


exports.customCallable = functions.https.onCall((data, context) => {
  const firstName = data.firstName;
  const lastName = data.lastName;

  console.log("Given first and last names, the full name should be "
    + firstName + " " + lastName);
      
  return {
     firstName : firstName,
     lastName : lastName,
     fullName : firstName + " " + lastName
   }
});



/** 
 * The function is by the UI (createAccount.kt file) after the user has filled
 * in the required information to create the account. The function will send a
 * code to the user's provided phone number (using firebase functionality)
 *
 * @param data
 * 	Contains the user's phone number and other information (ex name, username,
 * 	password etc.)
 * 
 * @param context
 * 	Used to authicate the UI calling the function.
 *
 * @return
 * 	A encrypted string containg user's information (from data), time out, timestamp
 * 	and the code firebase sent to the user's phone number.
 */
exports.sendCodeToPhone = functions.https.onCall((data, context) => {
  const timeOut = 60; // In seconds
  console.log("SendCodeToPhone function was called");
  // 'recaptcha-container' is the ID of an element in the DOM.
  var applicationVerifier = new firebase.auth.RecaptchaVerifier(
    'recaptcha-container');
  var provider = new firebase.auth.PhoneAuthProveder();
  provider.verifyPhoneNumber('+16505551234', applicationVerifier)
    .then((verificationId) => {
      console.log("Verification code sent");
	  var userVerificationCode = 0;
      return firebase.auth.PhoneAuthProvider.credential(verificationId,
          userVerificationCode);
    })
    .then((phoneCredential) => {
      console.log("User automatically verified");
      return firebase.auth().signInWithCredential(phoneCredential);
    })
    .catch((error) => {
      console.log("ERROR: Verification failed with error" + error);
	  const errorStatement = ['Failed. Try verification again', error, ]
      return Promise.resolve((errorStatement));
    });
});

  /****************************************************************************
  await auth.PhoneAuthProvider.getInstance().verifyPhoneNumber(
    data.phoneNumber,
    timeOut,
    timeOutUnit,
    new auth.PhoneAuthProvider.OnVerificationStateChangedCallbacks(() => {
      function onVerticationComplete(credential) {
        console.log("Automatic verification completed");
        //TODO
        return {
		  // TODO: Encrypt all the following string
          "userData": data.stringify(),
		  "timeOut": timeOut.toString(),
		  "credientials": code
		  "firebaseToken": token
      	  "error": "",
		  "verified": true
		  // TODO: Note the timestamp
		  // "timestamp": timestamp
        }
      }

      function onVericationFailed(e) {
        return {
          "error": "Retry request. Error: " + e
        }
      }

      function onCodeSent(code, token) {
        return {
		  // TODO: Encrypt all the following string
          "userData": data.stringify(),
		  "timeOut": timeOut.toString(),
		  "credientials": code
		  "firebaseToken": token
      	  "error": "",
		  "verified": false
		  // TODO: Note the timestamp
		  // "timestamp": timestamp
        }
      )};
    });
  ****************************************************************************/


/**
 * The function is responsible for creating a new user account by
 * validating that the code sent by the user matches the code send by
 * firebase. If the condition is met, the function creates a user in
 * firebase users database and firebase provided user table.
 *
 * @param data
 * 	Contains three general things:
 *  	(i)   User code: the code entered by the user to verfiy their phone
 *		      phone number.
 *	    (ii)  User data: A encrypted string containing the user's data (ex.
 *			  name, username, password etc.)
 * 		(iii) Firebase code: The code firebase sent the user along with a token.
 *
 * @param context
 * 	Used to authinticate the UI calling the function.
 *
 * @return
 * 	If the user code matches firebase code AND the timeOut limit has not 
 * 	been exceeded, the user is created based on the information in user
 * 	data.
 *	Else, the function issues a retry of phone validation.
 */
/****************************************************************************
exports.validatePhoneForSignUp = functions.https.onCall((data, context) => {
  if (!data.userData.verified && praseInt(data.userData.timeOut) + praseInt(data.userData.timeStamp) < currentTimeStamp) {
	// Time limit for phone validation was exceeded. Retry the whole operation.
  	const error = "Retry request. Error: Time limit exceeded"
    return Promise.resolve((error));
  }

  if (data.userData.verfified || data.userCode.localeCompare(data.firebaseCode) == 0) { // Both codes match
    // TODO: Create user

    // The following will create a entry in the users database
    await admin.firestore().collection('users').add {
      name: userData.name,
      username: userData.userName,
      password: userData.password,
      phoneNumber: userData.phoneNumber
	  // TODO: Store user id from firebase
      // NOTE: Do NOT store information that firebase will store in auth
      // TODO: Anything else that needs to be stored
    }

    // The following will create a entry in Firebase Authentication 
    await admin.auth().createUser({
      displayName: userData.name,
      phoneVerified: true,
      username: userData.userName,
      password: userData.password,
      phoneNumber: userData.phoneNumber
      // TODO: Anything else that firebase specifically stores/needs
    });
  }

  const error = "Retry request. Error: Wrong code supplied by user"
  return Promise.resolve((error));
});
****************************************************************************/

